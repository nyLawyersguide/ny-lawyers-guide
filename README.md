Through a couple different incarnations, New York Lawyers Guide has served as the area's No. 1 legal resource since 2002. 

As opposed to other national websites, this is made specifically for someone in the New York-Metropolitan area. That's because we have found, through our years of different print books and websites, that the best way for a prospective client to find the correct lawyer is twofold: 

1- The lawyer specializes in the needed Practice Area

2- The lawyer is located in the specific Location

That is why we have established the search function at the top of every page to include those two factors. Combining them will get you to the place where we're sure you're going to find the right attorney for your specific situation.

Over the years, we have also compiled a top-notch database of area attorneys, as well as some vital information for someone who is in the market for an attorney but needs to know a little bit more. It is all there for you, and it will continue to grow as we continue to serve the area as the best place to find the correct lawyer.

Finallywhen you have made your decision and hired an attorney or law firm, please let them know that you found them in the New York Lawyers Guide.
